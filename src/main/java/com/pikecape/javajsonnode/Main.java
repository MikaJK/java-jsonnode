package com.pikecape.javajsonnode;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Main
{
    public static void main(String[] args) throws JsonProcessingException {
        String nullJson = null;
        String emptyJson = "{}";
        String jsonWithContent = "{\"data\": {\"name\": \"Duey\"}}";
        String jsonWithEmptyContent = "{\"data\": \"\"}";
        String jsonWithNullContent = "{\"data\": null}";
        String jsonWithoutDataField = "{\"duey\" : \"tupu\"}";

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode jsonNode = objectMapper.readTree(jsonWithNullContent);

        if (jsonNode.isNull()) {
            System.out.println("JsonNode is NULL.");
        } else {
            System.out.println("JsonNode is not NULL.");

            System.out.println(jsonNode.toString());

            if (!jsonNode.hasNonNull("data")) {
                System.out.println("JsonNode does not have \"data\" entry.");
            } else {
                System.out.println("JsonNode does have \"data\" entry.");

                //if (jsonNode.get("data").isNull()) {
                if (!jsonNode.hasNonNull("data")) {
                    System.out.println("JsonNode does not have \"data\" entry with NULL content.");
                } else {
                    System.out.println("JsonNode does have \"data\" entry.");

                    if (jsonNode.get("data").isEmpty()) {
                        System.out.println("Field \"data\" in JsonNode is empty.");
                    } else {
                        System.out.println("Field \"data\" in JsonNode is not empty.");

                        if (jsonNode.at("/data").get("name").isNull()) {
                            System.out.println("Field \"data.name\" does not exist.");
                        } else {
                            System.out.println("Field \"data.name\" does exist.");

                            if (jsonNode.at("/data").get("name").asText().isEmpty()) {
                                System.out.println("Field \"data.name\" is empty.");
                            } else {
                                System.out.println("Field \"data.name\" is not empty.");

                                System.out.println("name: " + jsonNode.at("/data").get("name"));
                            }
                        }
                    }
                }
            }

        }




    }
}
